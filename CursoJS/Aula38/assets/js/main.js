const paragrafos = document.querySelector('.paragrafos');
const container = document.querySelector('.container')

const ps = paragrafos.querySelectorAll('p');

const estilosBody = getComputedStyle(document.body);
const estilosContainer = getComputedStyle(container);

const backgroundColorBody = estilosBody.backgroundColor;
const colorText = estilosContainer.backgroundColor;

console.log(backgroundColorBody);
console.log(colorText);

for (let p of ps) {
    p.style.backgroundColor = backgroundColorBody;
    p.style.color = colorText;
}