/**
 *  tipos primitivos (imutáveis)
 *  String, number, boolean, undefined, null, bigint, symbol - Valores copiados


let nome = 'Luiz';
nome = 'Otávio';
nome[0] = 'L'; // não funciona
console.log(nome);


let a = 'A';
let b = a; // copia do valor de a para b;
console.log(a, b);
a = 'Outra coisa';

console.log(a, b);
 */

/* Referência (mutável) - array, object, function - Passados por referência */

/*
let a = [1, 2, 3];
// por referencia
let b = a;
// aqui se copia o conteúdo para b;
// b = [...a];
let c = b;
console.log(a, b);
a.push(4);
console.log(a, b);
b.push(5);
console.log(a, b);
b.pop();
console.log(a, b, c);
*/
/*
const a = {
    nome: 'Luiz',
    sobrenome: 'Otávio'
}

const b = a;
// copia o valor para b
// b = {...a}
b.nome = 'Fernando';
console.log(a, b);

*/