let num1 = 1;
let num2 = 2.5;
let num3 = 10.23490902;

console.log(num1 + num2);
// faz a concatenação
console.log(num1.toString() + num2);
// continua sendo um number
console.log(typeof num1);
// convertendo para string definitivamente
//num1 = num1.toString();
// numero binario
console.log(num1.toString(2));

// arredondamento
console.log(num3.toFixed(2));

// verificar se é inteiro retorna um booleano (true ou false)
console.log(Number.isInteger(num3));

let temp = num1 * 'Olá!';
// verifica se o numero ou expressão é válido ou é um numero
console.log(Number.isNaN(temp));

// IEEE 754-2008


let num4 = 0.7;
let num5 = 0.1;

console.log(num4 + num5);

num4 += num5;
num4 += num5;
num4 += num5;
num4.toFixed(2);

console.log(num4);

//num4 = parseFloat(num4.toFixed(2));
num4 = Number(num4.toFixed(2));

console.log(num4);