// função MAP

/* const numeros = [5, 50, 80, 1, 2, 3, 5, 8, 7, 11, 15, 22, 27];

const dobro = numeros.map(valor => valor * 2);

console.log(dobro);
 */
const pessoas = [{ nome: 'Luiz', idade: 62 }, { nome: 'Antonio', idade: 53 }, { nome: 'Maria', idade: 35 }, { nome: 'Regina', idade: 61 }, { nome: 'Ladislau', idade: 55 }, { nome: 'Luiza', idade: 27 }, { nome: 'Amanda', idade: 15 }];


/* const nomes = pessoas.map(obj => obj.nome);
console.log(nomes); */

/* const idades = pessoas.map(obj => obj.idade);
console.log(idades); */

const pessoasComId = pessoas.map((obj, indice) => ({ id: indice, nome: obj.nome, idade: obj.idade }));
/* const pessoasComId = pessoas.map(function(obj, indice) {
    (obj.id = indice + 1);
    return obj;
}); */


console.log(pessoas);
console.log(pessoasComId);
console.log(pessoas);