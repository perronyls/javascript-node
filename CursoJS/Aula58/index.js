/**
 * Função construtora retorna objeto -> objects
 * Função fabrica -> Objects -> factory
 */

function Pessoa(nome, sobrenome) {
    // atributos ou métodos privados
    const ID = 123;

    const metodoInterno = function() {
        /**
         * ... código
         */

    };

    // atributos ou métodos públicos
    this.nome = nome;
    this.sobrenome = sobrenome;

    this.metodo = () => {
        console.log(this.nome + ' Sou um método');
    }
}

const p1 = new Pessoa('Ladislau', 'Perrony');
const p2 = new Pessoa('Regina', 'Perrony');

console.log(p1.nome);
console.log(p2.nome);

p1.metodo();