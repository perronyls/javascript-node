/**
 * Essa é a verificação de sequencias '111.111.111-11' até '999.999.999-99'
 * Retorno um array de um filter de todos os elementos que cuja divisão pelo 
 * primeiro elemento do array filtrado seja diferente de 1
 */
function validaSeq(cpf) {
    return cpf.filter(valor => valor / cpf[0] !== 1);
}


function sizeCpf(cpf) {
    return cpf.length !== 11
}


function soma(cpf, n) {
    const soma = cpf.reduce(function(ac, valor, indice) {
        ac += (Number(valor) * (n - indice));
        return ac;
    }, 0);
    return soma;
}

function verificador(soma) {
    if (11 - (soma % 11) > 9) {
        return '0';
    }
    return (11 - (soma % 11));
}

function validaCPF(nCpf) {
    /**
     * Nada como um debugger para debugar no navegador.
     * Depois esquecer no meio do código em produção! kkkkkkkk
     *
     */
    // debugger;
    //const nCpf = '605.484.450-52';
    const cpf = nCpf.replace(/\D+/g, '');
    const cpfArray = Array.from(cpf).slice(0, -2);

    /** se a verificação retornar um array vazio, trata-se de uma sequência */
    if (validaSeq(Array.from(cpf)).length === 0) return false;

    /** se o comprimento da string for menor que 11 já encerro aqui */
    if (sizeCpf(cpf)) return false;

    /** retorno o tamanho do array  */
    let digito = cpfArray.push(verificador(soma(cpfArray, 10)));

    /** aqui se o primeiro dígito não corresponder já encerro a função */
    if (cpf.substr(9, 1) !== cpfArray[digito - 1].toString()) return false;

    digito = cpfArray.push(verificador(soma(cpfArray, 11)));

    /** aqui comparo o CPF inteiro, se for diferente retorno falso */
    if (cpfArray.join('') !== cpf) return false;

    return true;

};




function Verifica() {

    const inputCpf = document.querySelector('.cpf');
    this.init = () => {
        this.capturaClick();
    }


    this.displayMsg = (msg, cor) => {
        const div = document.querySelector('.alert');
        div.innerText = msg;
        div.style.backgroundColor = cor;
    };


    this.capturaClick = () => {
        document.addEventListener('click', event => {
            const el = event.target;
            if (el.classList.contains('btn-click')) {
                if (!validaCPF(inputCpf.value)) {
                    this.displayMsg('CPF Inválido', '#FF0000');
                    return;
                };
                this.displayMsg('CPF Válido', '#008000');


            }

        })

    };

}

const valida = new Verifica();
valida.init();