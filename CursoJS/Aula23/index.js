/**
 * && todas as expressões devem ser verdadeiras para retornar true
 * || todas as expressões devem ser falsas para retornar falso
 * 
 * false = false
 * 0 = false
 * '' "" `` = false (string vazia)
 * null / undefined = false
 * NaN = false
 * 
 *  Short-Circuit
 */



// console.log('Luiz Otávio' && '' && 'Maria');

/*
function falaOi() {
    return 'Oi';
}

const vaiExecutar = true;

console.log(vaiExecutar && falaOi());

*/

// console.log(0 || false || null || 'Luiz Otávio' || true);

/*
const corUsuario = null;
const corPadrao = corUsuario || 'Preto';

console.log(corPadrao);
*/

const a = 0;
const b = null;
const c = 'false';
const d = false;
const e = NaN;

// retorna 'false' mas a string false

console.log(a || b || c || d || e);