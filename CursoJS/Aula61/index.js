function* geradora1() {

    yield 'Valor1';
    yield 'Valor2';
    yield 'Valor3';

}

const g1 = geradora1();
/* console.log(g1.next());
console.log(g1.next().value);
console.log(g1.next().value);
console.log(g1.next()); */

/* for (let valor of g1) {
    console.log(valor);
} */

/* function* geradora2() {
    let i = 0;
    while (true) {
        yield i;
        i++;
    }
}

const g2 = geradora2();
console.log(g2.next().value); */

/* function* geradora3() {
    yield 0;
    yield 1;
    yield 2;
}

function* geradora4() {
    yield* geradora3();
    yield 3;
    yield 4;
    yield 5;
}

const g4 = geradora4();

for (let valor of g4) {
    console.log(valor);
}
 */

function* geradora5() {
    yield() => {
        console.log('Vim do yield 1');
    }

    yield() => {
        console.log('Vim do yield 2');
    }

    yield() => {
        console.log('Vim do yield 3');
    }
}

const g5 = geradora5();

for (let index of g5) {
    console.log(index);
}