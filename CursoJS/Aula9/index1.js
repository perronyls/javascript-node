// String, number, undefined, null, boolean, symbol

/* Strings */
const nome = 'Perrony'; 
const nome1 = "Perrony";
const nome2 = `Perrony`;

/* Number */boolean
const num1 = 10;
const num2 = 10.20;

/* Undefined e nulo */
let nomeAluno; // undefined -> indefinido, não aloca valor na memória -> Não pode ser constante
const sobreNome = null; // Nulo -> Define uma variável como nulo.

/* valores lógicos booleanos -> true ou false */
const boolean = false;
const boolean1 = true;

console.log(typeof nome, nome, typeof num1, num1, typeof nomeAluno,nomeAluno, typeof sobreNome,sobreNome, typeof boolean,boolean );






