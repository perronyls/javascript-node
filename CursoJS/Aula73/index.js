/**
 * Object.values
 * Object.entries
 * Object.assign(des,any)
 * Object.getOwnPropertyDescriptor(o,'prop') ... (spread)
 * 
 * // Já vimos
 * Object.keys(retorna chaves)
 * Object.freeze (congela o objeto)
 * Object.defineProperties(define várias propriedades)
 * Object.defineProperty (define uma propriedade)
 */

const produto = { nome: 'Caneca', preco: 1.8, material: 'Porcelana' };
// espalhando (spread) objeto e incluindo outras propriedades
/* const caneca = {
    ...produto,
    material: 'Porcelana'
}; */

/**
 * const caneca = Object.assign({}, produto, { material: 'Porcelana' });

 */

/**
 * Forma manual
 * const caneca = { nome: produto.nome, preco: produto.preco, materia: 'Porcelana' };

 */

/* caneca.nome = 'Caneca Nova';
caneca.preco = 2.5; */
/* console.log(produto); */
/* Object.defineProperty(produto, 'nome', {
    writable: false,
    configurable: false,
    value: 'Qualquer outra coisa'
});
console.log(Object.getOwnPropertyDescriptor(produto, 'nome')); */

/* console.log(Object.values(produto));
console.log(Object.entries(produto)); */

/* for (let entry of Object.entries(produto)) {
    console.log(entry);
} */

for (let [chave, valor] of Object.entries(produto)) {
    console.log(chave, valor);
}