// arrays e strings são indexadas
const alunos = ['Ladislau', 'Regina', 'Anderson', 'João'];

// editar um elemento de um array
// alunos[0] = 'Eduardo';
// console.log(alunos[2]);
// alunos[4] = 'Maria Luiza';
// console.log(alunos.length);

// console.log(alunos);
alunos.push('Luiza'); // adiciona no fim

//console.log(alunos);
// adiciona no inicio do array
alunos.unshift('Roberto');
//console.log(alunos);

// remove último elemento e salva o valor
//const removido = alunos.pop();
//console.log(alunos, removido);
// remove primeiro elemento e salva o valor
//const salvo = alunos.shift();
//console.log(alunos, salvo);
// apaga o valor do elemento do array, mas mantém o índice
//delete alunos[2];

console.log(alunos);

novoArray = alunos.slice(0, -2);
console.log(novoArray);
console.log(alunos instanceof Array);

novoArray[33] = 'Teste';
console.log(novoArray);