 /**
  *  Some todos os números (reduce)
  *  Retorne um array com os pares (Filter)
  *  Retorne um array com o dobro dos valores (map)
  */

 const numeros = [5, 50, 80, 1, 2, 3, 5, 8, 7, 11, 15, 22, 27];
 /* 
  const total = numeros.reduce(function(acumulador, valor, indice, array) {
      acumulador += valor;
      return acumulador;
  }, 0);

  console.log(total); */

 /*  const total = numeros.reduce(function(acumulador, valor, indice, array) {
      if (valor % 2 === 0) acumulador.push(valor);
      return acumulador;
  }, []); */


 /*  const total = numeros.reduce(function(acumulador, valor, indice, array) {
      acumulador.push(valor * 2);
      return acumulador;
  }, []); */

 /*  const total = numeros.reduce(function(acumulador, valor, indice, array) {
      if (valor % 2 === 0)
          acumulador += valor;
      return acumulador;
  }, 0);

  console.log(total); */

 const pessoas = [{ nome: 'Luiz', idade: 62 }, { nome: 'Antonio', idade: 73 }, { nome: 'Maria', idade: 35 }, { nome: 'Regina', idade: 61 }, { nome: 'Ladislau', idade: 55 }, { nome: 'Luiza', idade: 27 }, { nome: 'Amanda', idade: 15 }];


 const maisIdade = pessoas.reduce(function(acumulador, valor) {
     if (acumulador.idade > valor.idade) return acumulador;
     return valor;

 });

 console.log(maisIdade);