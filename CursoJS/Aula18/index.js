// objetos
/*
const nome01 = 'Luiz';
const sobrenome01 = 'Miranda';
const idade01 = 25;


const nome02 = 'Ladislau';
const sobrenome02 = 'Perrony';
const idade02 = 55;


const pessoa1 = {
    nome: 'Luiz',
    sobrenome: 'Miranda',
    idade: 25
}

console.log(pessoa1.nome);
console.log(pessoa1.sobrenome);
console.log(pessoa1.idade);


function criaPessoa(nome, sobrenome, idade) {
    return {
        nome,
        sobrenome,
        idade
    }
}

const pessoa1 = criaPessoa('Luiz', 'Miranda', 25);
const pessoa2 = criaPessoa('Miranda', 'Alves', 46);
const pessoa3 = criaPessoa('Regina', 'Correa', 60);
const pessoa4 = criaPessoa('Maria', 'Luiza', 25);
const pessoa5 = criaPessoa('Ladislau', 'Perrony', 55);

console.log(pessoa1);

*/

const pessoa1 = {
    nome: 'Luiz',
    sobrenome: 'Miranda',
    idade: 25,
    fala() {
        console.log(`${this.nome} está falando oi...`);
    },
    incrementaIdade() {
        ++this.idade;
    }
};

pessoa1.fala();
pessoa1.incrementaIdade();
console.log(pessoa1.idade);