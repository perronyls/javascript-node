class ControleRemoto {
    constructor(tv) {
        this.tv = tv;
        this.volume = 0;
    }

    // método de instância
    aumentarVolume() {
        this.volume += 2;
    }

    // método de instância
    diminuirVolume() {
        this.volume -= 2;
    }

    // método estático
    // uma vez que não chama o construtor um método estático não possui acesso as propriedades da classe
    static soma(x, y) {
        return x + y;
    }

}


const controle1 = new ControleRemoto('LG');
controle1.aumentarVolume();
controle1.aumentarVolume();
controle1.aumentarVolume();
console.log(controle1);


console.log(ControleRemoto.soma(2, 3));