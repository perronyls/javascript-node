/* try {
    console.log(naoExisto);

} catch (err) {
    console.log('Erro interno');
} */

function soma(x, y) {
    if (typeof x !== 'number' || typeof y !== 'number') {
        throw new Error('X ou Y precisam ser números');
    }
    return x + y;
}

try {
    console.log(soma(1, 2));
    console.log(soma('1', 2));
} catch (e) {
    console.log(e);
}