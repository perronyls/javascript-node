function calculaImc() {

    const form = document.querySelector('.form');

    const div = document.querySelector('.resultado');

    const resultados = [
        'Abaixo do peso',
        'Peso normal',
        'Sobrepeso',
        'Obesidade grau 1',
        'Obesidade grau 2',
        'Obesidade grau 3',
        'Digite um número para o peso',
        'Digite um número para altura'
    ];

    function getResultados(valor) {
        return resultados[valor];
    }

    function displayResultado(valor, imc, color) {
        if (imc) {
            div.innerHTML = `Seu IMC é ${imc} - ${getResultados(valor)}`;
        } else {
            div.innerHTML = `${getResultados(valor)}`;
        }

        if (color) {
            div.style.backgroundColor = color;
        } else {
            div.style.backgroundColor = 'red';
        }
    }

    function calculaImc(peso, altura) {
        return peso / (altura * altura);
    }

    function recebeEventoForm(e) {
        e.preventDefault();
        const inputPeso = form.querySelector('.peso');
        const inputAltura = form.querySelector('.altura');
        let nPeso = inputPeso.value;
        let nAltura = inputAltura.value;
        let imc;
        debugger;
        /* Converte para ponto se for digitado uma vírgula */
        if (nPeso.indexOf(',') > 0) {
            nPeso = nPeso.replace(',', '.');
        }

        if (nAltura.indexOf(',') > 0) {
            nAltura = nAltura.replace(',', '.');
        }
        if (nAltura.indexOf(".") == -1) {
            nAltura /= 100;
        }

        nPeso = Number(nPeso);
        nAltura = Number(nAltura);

        /* Valida se é um número para o peso */
        if (!nPeso || nPeso == 0) {
            return displayResultado(6);
            /* Valida se é um número para a altura */
        } else if (!nAltura || nAltura == 0) {
            return displayResultado(7);
        } else {

            /* Calculo do IMC */
            imc = Number(calculaImc(nPeso, nAltura)).toFixed(2);

            if (imc >= 39.9) return displayResultado(5, imc, '#800000');
            if (imc >= 34.9) return displayResultado(4, imc, '#B22222');
            if (imc >= 29.9) return displayResultado(3, imc, '#FF0000');
            if (imc >= 24.9) return displayResultado(2, imc, '#1E90FF');
            if (imc >= 18.5) return displayResultado(1, imc, '#008000');
            if (imc < 18.5) return displayResultado(0, imc, '#FF4500');

        }
    }


    form.addEventListener('submit', recebeEventoForm);
}

calculaImc();