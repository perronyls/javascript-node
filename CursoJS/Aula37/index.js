const nome = ['Ladislau', 'Scheffer', 'Perrony'];

/* 
 *** for clássico, maneira antiga ***
for (let i = 0; i < nome.length; i++) {
    console.log(nome[i]);


    *** for in onde se informa um índice ***
for (let i in nome) {
    console.log(nome[i]);
}
} 

*** for of, onde traz apenas o valor
for (let valor of nome) {
    console.log(valor);
}
*/
/* 

/* Não funciona com string, apenas com array ou objetos */
/* nome.forEach(elemento => {
    console.log(elemento);
});
 */
/* Não funciona com string, apenas com array ou objetos */
/* nome.forEach(function(valor, indice, array) {
    console.log(valor, indice, array);
}); */