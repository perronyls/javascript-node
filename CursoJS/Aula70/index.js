/* const pessoa = {
    nome: 'Ladislau',
    sobrenome: 'Scheffer perrony',

}
 */
/* console.log(pessoa.nome);
console.log(pessoa.sobrenome);
console.log(pessoa['nome']);
console.log(pessoa['sobrenome']);

const chave = 'nome';
const outraChave = 'sobrenome';
console.log(pessoa[chave]);
console.log(pessoa[outraChave]); */

/* const pessoa = new Object();
pessoa.nome = 'Ladislau';
pessoa.sobrenome = 'Scheffer Perrony';

console.log(pessoa.nome);
console.log(pessoa.sobrenome);
console.log(pessoa['nome']);
console.log(pessoa['sobrenome']);

const chave = 'nome';
const outraChave = 'sobrenome';
console.log(pessoa[chave]);
console.log(pessoa[outraChave]); */

/* const pessoa = new Object();
pessoa.nome = 'Ladislau';
pessoa.sobrenome = 'Scheffer Perrony';
pessoa.idade = 55;
pessoa.falarNome = function() {
    console.log(`${this.nome} está falando seu nome.`)
}
pessoa.getDataNascimento = function() {
    const dataAtual = new Date();
    return dataAtual.getFullYear() - this.idade;
}

console.log(pessoa.getDataNascimento());

for (let chave in pessoa) {
    console.log(chave, typeof chave);
    console.log(pessoa[chave]);
} */

/* console.log(pessoa);
delete pessoa.nome;
console.log(pessoa); */

/* function criaPessoa(nome, sobrenome) {
    return {
        nome,
        sobrenome,
        get nomeCompleto() {
            return `${this.nome} ${this.sobrenome}`;
        }
    }
}

const pessoa1 = criaPessoa('Ladislau', 'Scheffer');
console.log(pessoa1.nomeCompleto); */

function Pessoa(nome, sobrenome) {
    this.nome = nome;
    this.sobrenome = sobrenome;
    // congela estrutura do objeto
    Object.freeze(this);
}

const p1 = new Pessoa('Ladislau', 'Scheffer');
console.log(p1);