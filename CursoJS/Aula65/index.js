const numeros = [5, 50, 80, 1, 2, 3, 5, 8, 7, 11, 15, 22, 27];


/* const filtrados = (numero) =>
    numero > 10;

const filtro = numeros.filter(filtrados);

console.log(filtro); */

//* filter sempre vai retornar um array com as opções do filtro;
//* neste caso passamos o valor, índice e o array que vai ser iterado.
/* const numerosFiltrados = numeros.filter((n, i, a) => {
    console.log(n, i, a);
    return a > 10;
})
 */
/* const numerosFiltrados = numeros.filter((n) => n > 10);
console.log(numerosFiltrados); */

const pessoas = [{ nome: 'Luiz', idade: 62 }, { nome: 'Antonio', idade: 53 }, { nome: 'Maria', idade: 35 }, { nome: 'Regina', idade: 61 }, { nome: 'Ladislau', idade: 55 }, { nome: 'Luiza', idade: 27 }, { nome: 'Amanda', idade: 15 }];

const nomeGrande = pessoas.filter(valor => valor.nome.length >= 8);
const maiorIdade = pessoas.filter(valor => valor.idade >= 50);
const nomeEnda = pessoas.filter(valor => valor.nome.toLowerCase().endsWith('a'));
const nomeEndaMaiorIdade = pessoas.filter(valor => valor.nome.toLowerCase().endsWith('a') && valor.idade < 18);

console.log(nomeGrande);
console.log(maiorIdade);
console.log(nomeEnda);
console.log(nomeEndaMaiorIdade);