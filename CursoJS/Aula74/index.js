/**
 * Javascript é baseado em protótipos para passar propriedades e métodos
 * de um objeto para outro.
 * 
 * Definição de protótipo (prototypes)
 * Protótipo é o termo usado para se referir ao que foi criado pela
 * primeira vez, servindo de modelo ou molde para futuras produções.
 * 
 * Todos os objetos tem uma referência interna para um protótipo (__proto__)
 * que vem da propriedade prototype da função construtora que foi usada para
 * criá-lo. Quando tentamos acessar um membro de um objeto, primeiro o motor
 * do JS vai tentar encontrar este membro no próprio objeto e depois a cadeia
 * de protótipos é usada até o topo (null) até encontrar (ou não) tal membro. 
 
 */


function Pessoa(nome, sobrenome) {
    this.nome = nome;
    this.sobrenome = sobrenome;
    Pessoa.prototype.nomeCompleto = () => this.nome + ' ' + this.sobrenome;

}

const p1 = new Pessoa('Ladislau', 'Perrony');
const p2 = new Pessoa('Regina', 'Correa');
const data = new Date();
const dataNova = {...data, teste: 'teste' };

/* Pessoa.prototype.estouAqui = 'Hahahahahahaha';
Pessoa.prototype.nomeCompleto = function() {
    return this.nome + ' ' + this.sobrenome;
}; */

console.log(p1);
console.log(p2);
console.log(data);
console.log(dataNova);