const pessoa = {
    nome: 'Ladislau',
    sobrenome: 'Perrony',
    idade: 55,
    endereco: {
        rua: 'José Francisco de Carvalho',
        numero: 125
    }
}

const { nome: teste = '', endereco: { rua: r = '', numero: n = 'sn' } } = pessoa;
console.log(r, n);