/*
function saudacao(nome) {
    return `Bom dia  ${nome}!`
}

const variavel = saudacao('Perrony');

console.log(variavel);


function soma(x = 1, y = 1) {
    //constantes e variáveis podem ser criados com nome de outras variaveis de fora do escopo da função 
    const resultado = x + y;
    return resultado;
}

const resultado = soma(3);

console.log(resultado);


const raiz = function(n) {
    return Math.sqrt(n);
};



const raiz = (n) => {
    return Math.sqrt(n);
};



// arrow function
const raiz = n => Math.sqrt(n);
console.log(raiz(9));
*/
const soma = (x = 1, y = 2) => { return x + y };
console.log(soma(1, 4));

const subtrai = (x, y) => x - y;
console.log(subtrai(3, 1));