/**
 * 705.484.450-52  070.987.720-03
 * 
 * 7x 0x 5x 4x 8x 4x 4x 5x 0x
 * 10 9  8  7  6  5  4  3  2
 * 70 0  40 28 48 20 16 15 0 -> soma = 237
 *  * 11 - (237 % 11) = 5 (Primeiro Dígito) -> o Resultado for maior que 9 dígito é igual a 0

 * 7x 0x 5x 4x 8x 4x 4x 5x 0x 5x
 * 11 10 9  8  7  6  5  4  3  2x
 * 77 0  45 32 56 24 20 20 0  10 -> soma = 284
 *  * 11 - (284 % 11) = 5 (Segundo Dígito) -> o Resultado for maior que 9 o dígito é igual a 0
 * 
 * 
 */

function sizeCpf(cpf) {
    return cpf.length < 11
}

function soma(cpf, n) {
    const soma = cpf.reduce(function(ac, valor, indice) {
        ac += (Number(valor) * (n - indice));
        return ac;
    }, 0);
    return soma;
}

function verificador(soma) {
    if (11 - (soma % 11) > 9) {
        return '0';
    }
    return (11 - (soma % 11));
}

function validaCPF() {
    /**
     * Nada como um debugger para debugar no navegador.
     * Depois esquecer no meio do código em produção! kkkkkkkk
     *
     */
    // debugger;
    const nCpf = '605.484.450-52';
    const cpf = nCpf.replace(/\D+/g, '');
    const cpfArray = Array.from(cpf).slice(0, -2);

    /** se o comprimento da string for menor que 11 já encerro aqui */
    if (sizeCpf(cpf)) return false;

    /** retorno o tamanho do array  */
    let digito = cpfArray.push(verificador(soma(cpfArray, 10)));

    /** aqui se o primeiro dígito não corresponder já encerro a função */
    if (cpf.substr(9, 1) !== cpfArray[digito - 1].toString()) return false;

    digito = cpfArray.push(verificador(soma(cpfArray, 11)));

    /** aqui comparo o CPF inteiro, se for diferente retorno falso */
    if (cpfArray.join('') !== cpf) return false;

    return true;

};



const cpf = validaCPF();
if (cpf) {
    console.log('CPF Válido');
} else {
    console.log('CPF Inválido');
}