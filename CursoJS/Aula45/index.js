/* try {
    // quando não há erros
    console.log('Abri um arquivo');
    console.log('Manipulei o arquivo e gerou erro');
    console.log('Fechei o arquivo');

    try {
        console.log(a)
    } catch (e) {
        console.log('Deu erro');
    } finally {
        console.log('Sempre também')
    }
} catch (err) {
    // é executada quando há erros
    console.log('Tratando o erro');

} finally {
    // sempre
    console.log('Tratado o erro');
    console.log('Sempre será executado');
} */

function retornaHora(data) {
    if (data && !(data instanceof Date)) {
        throw new TypeError('Esperando instância de Date.');
    }
    if (!data) {
        data = new Date()
    }

    return data.toLocaleTimeString('pt-BR', {
        hour: '2-digit',
        minute: '2-digit',
        secont: '2-digit',
        hour12: false
    });

}

try {
    const data = new Date('01-01-1970 12:58:12');
    const hora = retornaHora(erro);

} catch (error) {
    console.log(error);

} finally {
    console.log('Tenha um bom dia.');
}