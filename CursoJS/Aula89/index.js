/* function rand(min, max) {
    min *= 1000;
    max *= 1000;
    return Math.floor(Math.random() * (max - min) + min);
}


function esperaAi(msg, tempo, cb) {
    setTimeout(() => {
        console.log(msg);
        if (cb) cb();
    }, tempo);
}

esperaAi('Frase1', rand(1, 3), function() {
    esperaAi('Frase2', rand(1, 3), function() {
        esperaAi('Frase3', rand(1, 3));
    });

}); */


function rand(min, max) {
    min *= 1000;
    max *= 1000;
    return Math.floor(Math.random() * (max - min) + min);
}

function esperaAi(msg, tempo) {
    return new Promise((resolve, reject) => {


        setTimeout(() => {
            if (typeof msg !== 'string') {
                reject('BAD VALUE');
                return;
            }
            resolve(msg.toUpperCase() + ' - Passei na promise');
        }, tempo);
    });

}

// Promise.all Promise.race Promise.resolve Promise.reject

const promisses = [

    //'Primeiro valor',
    esperaAi('Promise 1', 3000),
    esperaAi('Promise 2', 500),
    esperaAi('Promise 3', 1000),
    //esperaAi(3, 1000),
    //'Outro valor'

];

/* Promise.all(promisses)
    .then(valor => {
        console.log(valor);
    }).catch(e => {
        console.log(e)
    }); */


// no race vai ser o primeiro a ser resolvido
/* Promise.race(promisses)
    .then(valor => {
        console.log(valor);
    }).catch(e => {
        console.log(e)
    });
 */

function baixaPagina() {
    const emCache = false;

    if (emCache) {
        return Promise.resolve('Página em cache');
    }
    return esperaAi('Baixei a página', 3000);
}

baixaPagina().then(dadosPagina => {
    console.log(dadosPagina);
}).catch(e => console.log(e));