/* Operador ternário 
 * (condição) ? 'True': 'False'
 */

const pontuacaoUsuario = 1005;

const nivelUsuario = pontuacaoUsuario >= 1000 ? 'Usuario VIP' : 'Usuário Normal';

const corUsuario = null;
const corPadrao = corUsuario || 'Preta';


console.log(nivelUsuario, corPadrao);