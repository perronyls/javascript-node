// defineProperty - defineProperties

function Produto(nome, preco, estoque) {


    /* Object.defineProperty(this, 'estoque', {
        enumerable: true, // mostra a chave
        value: estoque, // valor da chave
        writable: true, // permite alterar o valor
        configurable: false // permite reconfigurar ou apagar a chave

    }); */

    Object.defineProperties(this, {
        nome: {
            enumerable: true, // mostra a chave
            value: nome, // valor da chave
            writable: true, // permite alterar o valor
            configurable: false // permite reconfigurar ou apagar a chave

        },
        preco: {
            enumerable: true, // mostra a chave
            value: preco, // valor da chave
            writable: true, // permite alterar o valor
            configurable: false // permite reconfigurar ou apagar a chave

        },
        estoque: {
            enumerable: true, // mostra a chave
            value: estoque, // valor da chave
            writable: true, // permite alterar o valor
            configurable: false // permite reconfigurar ou apagar a chave
        }
    });

}

const p1 = new Produto('Camiseta', 20, 30);
p1.estoque = 1000;
console.log(p1);

console.log(Object.keys(p1));