/**
 *  Declaração de função (function hoisting)
 */

falaOi();

function falaOi() {
    console.log('Oi');
}

/**
 * First-class objects (Objetos de primeira classe)
 * Function expressin
 */

const souUmDado = function() {
    console.log('Sou um dado!');
}

souUmDado();

function executaFuncao(funcao) {
    console.log('Vou executar a função abaixo!');
    funcao();
}

executaFuncao(souUmDado);

/**
 * Arrow Function
 */

const funcaoArrow = () => {
    console.log('Sou uma arrow function');
}

funcaoArrow()

/**
 * Dentro de um objeto posso ter uma função
 */


const obj = {
    falar: function() {
        console.log('Estou falando...');
    },
    fala: () => console.log('Uma arrow dentro do objeto')


}

obj.falar();
obj.fala();