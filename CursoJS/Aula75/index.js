/* const objA = {
    chaveA: 'A'
};

const objB = {
    chaveB: 'A'
};

const objC = new Object();
objC.chaveC = 'C'


Object.setPrototypeOf(objB, objA);
Object.setPrototypeOf(objC, objB);
 */

function Produto(nome, preco) {
    this.nome = nome;
    this.preco = preco;
}

Produto.prototype.desconto = function(desc) {
    this.preco = this.preco - (this.preco * (desc / 100));
}

Produto.prototype.acrescimo = function(acres) {
    this.preco = this.preco + (this.preco * (acres / 100));
}

const p1 = new Produto('Camiseta', 50);
/* p1.desconto(10);
console.log(p1);
p1.acrescimo(10);
console.log(p1); */

const p2 = {
    nome: 'Caneca',
    preco: 15
}

Object.setPrototypeOf(p2, Produto.prototype);

/* p2.acrescimo(10);
console.log(p2); */

const p3 = Object.create(Produto.prototype, {
    tamanho: {
        writable: false,
        configurable: true,
        enumerable: true,
        value: 42
    }
});
p3.preco = 17;
p3.nome = 'Meias';

p3.acrescimo(10);
console.log(p3);