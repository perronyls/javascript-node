/**
 *  Operadores de comparação
 *  > Maior que
 *  >= Maior ou igual a
 *  < Menor que
 *  <= Menor ou igual a
 *  == igualdade (não recomendado o uso)
 *  === igualdade restrita (valor e tipo)
 *  != diferente (não recomendado o uso)
 *  !== diferente restrito (valor e tipo)
 */

// const comp = 10 >= 11; false
// const comp = 10 > 5;  true
// const comp = 10 > 10; false
// const comp = 10 < 10; false
// const comp = 10 == '10'; true
// const comp = 10 === '10'; false
// const comp = 10 === 10; true
// const comp = 10 != 10; false
// const comp = 10 != '10'; false
// const comp = 10 !== '10'; true
console.log(comp);