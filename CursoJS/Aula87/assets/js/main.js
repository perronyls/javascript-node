class ValidaFormulario {
    constructor() {
        this.form = document.querySelector('.form');
        this.evento();
    }

    evento() {
        this.form.addEventListener('submit', e => {
            this.handleSubmit(e);
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        const camposValidos = this.isValid();
        const senhasValidas = this.senhasSaoValidas();

        if (camposValidos && senhasValidas) {
            alert('Formulário enviado!');
            this.form.submit();
        }

    }

    senhasSaoValidas() {
        let valid = true;
        const senha = this.form.querySelector('.senha');
        const confirma = this.form.querySelector('.confirme');

        if (senha.value !== confirma.value) {
            this.criaErro(senha, 'Campos senha e confirmação devem ser iguais');
            this.criaErro(senha, 'Campos senha e confirmação devem ser iguais');
            valid = false;
        }

        if (senha.value.length < 3 || senha.value.length > 12) {
            valid = false;
            this.criaErro(senha, 'Senha deve ter entre 3 e 12 caracteres');
        }
        return valid;
    }

    isValid() {
        let valid = true;
        for (let errorText of this.form.querySelectorAll('.error-text')) {
            errorText.remove();
        }
        for (let campo of this.form.querySelectorAll('.form-control')) {
            if (!campo.value) {
                const label = campo.previousElementSibling.innerHTML;
                this.criaErro(campo, `Campo "${label}" não pode estar em branco`);
                valid = false;
            }
            if (campo.classList.contains('cpf')) {
                if (!this.validaCPF(campo)) valid = false;
            }
            if (campo.classList.contains('usuario')) {
                if (!this.validaUsuario(campo)) valid = false;
            }
        }
        return valid;
    }

    validaUsuario(campo) {
        const usuario = campo.value;
        let valid = true;

        if (usuario.length < 3 || usuario.length > 12) {
            this.criaErro(campo, 'Usuário precisa ter entre 3 e 12 caracteres');
            valid = false;
        }
        const regexp = '^[A-Za-z0-9]*[A-Za-z0-9][A-Za-z0-9]*$';
        //const regexp = '^[a-zA-Z0-9]+$/g';
        if (!usuario.match(regexp)) {
            this.criaErro(campo, 'Nome de usuário precisa ter apenas letras ou números');
            valid = false;
        }
        return valid;
    }

    validaCPF(campo) {
        const cpf = new ValidaCPF(campo.value);
        if (!cpf.valida()) {
            this.criaErro(campo, 'CPF Inválido');
        }
        return true;

    }

    criaErro(campo, msg) {

        const div = document.createElement('div');
        div.innerHTML = msg;
        div.classList.add('error-text');
        campo.insertAdjacentElement('afterend', div);

    }

}

const valida = new ValidaFormulario();