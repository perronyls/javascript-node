/**
 * classe para validar CPF
 */
class ValidaCPF {
    constructor(cpf) {
        Object.defineProperty(this, 'cpfLimpo', {
            enumerable: false,
            writable: false,
            configurable: false,
            value: cpf.replace(/\D+/g, '')
        });
    }

    validaSeq(cpf) {
        return cpf.filter(valor => valor / cpf[0] !== 1);
    }

    sizeCpf(cpf) {
        return cpf.length !== 11
    }

    soma(cpf, n) {
        const soma = cpf.reduce(function(ac, valor, indice) {
            ac += (Number(valor) * (n - indice));
            return ac;
        }, 0);
        return soma;
    }

    verificador(soma) {
        if (11 - (soma % 11) > 9) {
            return '0';
        }
        return (11 - (soma % 11));
    }

    valida() {
        const cpfArray = Array.from(this.cpfLimpo).slice(0, -2);
        if (this.validaSeq(Array.from(this.cpfLimpo)).length === 0) return false;
        if (this.sizeCpf(this.cpfLimpo)) return false;
        let digito = cpfArray.push(this.verificador(this.soma(cpfArray, 10)));
        if (this.cpfLimpo.substr(9, 1) !== cpfArray[digito - 1].toString()) return false;
        digito = cpfArray.push(this.verificador(this.soma(cpfArray, 11)));
        if (cpfArray.join('') !== this.cpfLimpo) return false;
        return true;

    };

};

class ValidaNome {
    constructor(nome) {
        Object.defineProperty(this, 'nomeLimpo', {
            value: nome.replace(/[a-z0-9]/gi, '')
        })
    };



}

class ValidaUsuario {

    constructor(usuario) {
        Object.defineProperty(this, 'usuarioLimpo', {
            enumerable: false,
            configurable: false,
            value: usuario.replace(/[a-z0-9]/gi, '')
        })
    }

    getUsuario() {
        return this.usuarioLimpo;
    }

    tamanho() {
        if (this.usuarioLimpo.length < 3 && this.usuarioLimpo.length > 12) return false;
        return true;

    }

}


function ValidaForm() {

    const form = document.querySelector('.form');
    const msg = document.querySelector('.msg');
    const inputNome = document.querySelector('.nome');
    /* const sobrenome = document.querySelector('.sobrenome');
    const inputCpf = document.querySelector('.cpf');
    const usuario = document.querySelector('.usuario');
    const senha = document.querySelector('.senha');
    const confirme = document.querySelector('.confirme'); */

    function recebe(e) {
        e.preventDefault();
        debugger;
        const validNome = new ValidaNome();
        if (validNome(inputNome.value)) return false;










    }

    form.addEventListener('submit', recebe);




}

const valida = ValidaForm();