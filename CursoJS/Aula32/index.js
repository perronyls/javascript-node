/* let a = 'A'; //B
let b = 'B'; //C
let c = 'C'; //A

console.log(a, b, c);

[a, b, c] = [b, c, a];

console.log(a, b, c); */

/* const numeros = [11, 22, 33, 44, 55, 66, 77, 88, 99];

const [um, , dois, ...resto] = numeros;

console.log(resto, um, dois); */

const numeros = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

const [, [, , a]] = numeros;
const [lista1, lista2, lista3] = numeros;
console.log(lista3[1]);