/**
 * Operadores Lógicos
 * && -> AND -> E (todas as expressões precisam ser verdadeiras para retornar verdadeiras)
 * || -> OR -> OU (se apenas uma das expressões for true retornará true)
 * ! -> NOT -> NÃO
 */

//const expressaoAnd = true && true && false && true;
// const expressaoOr = false || false || true;
// console.log(expressaoOr);

// const usuario = 'Ladislau';
// const senha = '123456';

// const vailogar = usuario === 'Ladislau' && senha === '123456';

const valor = true // true
const valor1 = !true // false
const valor2 = false // false
const valor3 = !false // true
const valor4 = !!false // false
const valor5 = !!true // true

console.log(valor5);