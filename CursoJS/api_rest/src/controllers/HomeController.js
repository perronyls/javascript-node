class HomeController {
    async index(req, res) {
        res.json('Index OK');
    }
}

export default new HomeController();
