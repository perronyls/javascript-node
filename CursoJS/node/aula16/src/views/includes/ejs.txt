<% Controle de fluxo (if,for...) %>
<%= Imprime escapando caracteres %>
<%- Imprime sem escapar caracteres %>
<# Comentário %>
<%- include('CAMINHO/ARQUIVO) Ex. head, footer %>

<% if(algumacoisa) { %>
    <%= exibe alguma coisa %>
<% } else { %>
    <%= exibe outra coisa %>
<% } %>

Documentação:
https://ejs.co/