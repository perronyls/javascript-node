const mongoose = require('mongoose');

const HomeSchema = new mongoose.Schema({
    titulo: { type: String, required: true },
    descricao: String
});

const HomeModel = mongoose.model('Home', HomeSchema);

/* module.exports = HomeModel; */


/* const HomeModel = require('../models/HomeModel');

HomeModel.create({
        
        titulo: 'Outro título de testes',
        descricao: 'Outra descrição de testes'
    }).then(dados => console.log(dados))
    .catch(e => console.log(e));
 */
/* HomeModel.find().then(dados => console.log(dados))
    .catch(e => console.log(e)); */

class Home {

}

module.exports = Home;