const express = require('express');

// CRUD -> CREATE, READ, UPDATE, DELETE
//         POST,   GET,  PUT,    DELETE

// DEFINIÇAO DE ROTAS
// http://meusite.com/ <- GET -> Entrega a página / 
// http://meusite.com/sobre <- GET -> Entrega a página /sobre 
// http://meusite.com/contato <- GET -> Entrega a página /contato 

const app = express();

app.get('/', (req, res) => {
    /* res.send('Hello World'); */
    res.send(`
        <form action="/" method="POST">
            Nome: <input type="text" name="nome">
            <button>Enviar</button>
        </form>
    `)
});

app.post('/', (req, res) => {
    res.send('Recebi o formulário');
});

app.get('/contato', (req, res) => {
    res.send('Obrigado por entrar em contato!');
})

app.listen(3000, () => {
    console.log('Acessar em http://localhost:3000')
    console.log('Servidor executando na porta 3000')
});