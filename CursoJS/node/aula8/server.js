const express = require('express');

// CRUD -> CREATE, READ, UPDATE, DELETE
//         POST,   GET,  PUT,    DELETE

// DEFINIÇAO DE ROTAS
// http://meusite.com/ <- GET -> Entrega a página / 
// http://meusite.com/sobre <- GET -> Entrega a página /sobre 
// http://meusite.com/contato <- GET -> Entrega a página /contato 

const app = express();

// trata os parametros de um post
app.use(express.urlencoded({ extended: true }));


app.get('/', (req, res) => {
    /* res.send('Hello World'); */
    res.send(`
        <form action="/" method="POST">
            Nome: <input type="text" name="nome">
            <button>Submeter formulário</button>
        </form>
    `)
});


app.get('/testes/:idUsuarios?/:Parametros?', (req, res) => {
    console.log(req.params);
    res.send(req.params);
});

// parametros em query ?teste=123&novo=654 = JSON -> queryString
app.get('/query', (req, res) => {
    res.send(req.query);
})


app.post('/', (req, res) => {
    console.log(req.body);

    res.send(`Recebi o formulário com o seguinte conteúdo ${req.body.nome}`);

});


app.listen(3000, () => {
    console.log('Acessar em http://localhost:3000')
    console.log('Servidor executando na porta 3000')
});