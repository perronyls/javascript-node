require('dotenv').config();

/* inicia o express */
const express = require('express');
const app = express();

/* inicia o mongoose para conectar com o mongo */
const mongoose = require('mongoose');
mongoose.connect(process.env.CONNECTIONSTRING, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Conectado!')
        app.emit('ok');
    }).catch(e => console.log(e));

/* inicia uma session */
const session = require('express-session');
/* seta a session na base de dados do mongo */
const MongoStore = require('connect-mongo')(session);

/* inicia o flash, para enviar mensagens autodestrutivas */
const flash = require('connect-flash');

/* carrega e define as rotas */
const routes = require('./routes');

const path = require('path');

/* Inicia módulo de segurança helmet */
const helmet = require('helmet');

/* Inicia o csrf token para incluir um token em todos os formularios como segurança */
const csrf = require('csurf');

/* carrega middlewares globais para ser inseridas nas rotas */
const { middlewareGlobal, checkCsrfError, csrfMiddleware } = require('./src/middlewares/middleware');

/* trata os parametros de um post */
app.use(express.urlencoded({ extended: true }));

/* permite enviar JSON para o backend da aplicação */
app.use(express.json());

/* Define o caminho para as pastas estáticas públicas (imagens, html,css, javascript */
app.use(express.static(path.resolve(__dirname, 'public')));

/* configuração de sessão */
const sessionOptions = session({
    secret: 'adfadsf',
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7,
        httpOnly: true
    }
});

app.use(sessionOptions);
app.use(helmet());
app.use(flash());
app.use(csrf());

/* seta o caminho das páginas html */
app.set('views', path.resolve(__dirname, 'src', 'views'));

/* define a engine ejs para renderizar as páginas */
app.set('view engine', 'ejs');

/* Nossos próprios middleware */
app.use(middlewareGlobal);
app.use(checkCsrfError);
app.use(csrfMiddleware);

/* usa as rotas definidas */
app.use(routes);

/* inicia a aplicação */
app.on('ok', () => {
    app.listen(3000, () => {
        console.log('Acessar em http://localhost:3000')
        console.log('Servidor executando na porta 3000')
    });
})