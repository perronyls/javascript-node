exports.paginaInicial = (req, res, next) => {
    /*  req.session.usuario = { nome: 'Luiz', logado: true } 
    console.log(req.session.usuario);*/

    /*  req.flash('info', 'Olá mundo!');
     req.flash('error', 'Info error');
     req.flash('success', 'Olá mundo successo!'); 

    console.log(req.flash('info'), req.flash('error'), req.flash('success'));*/

    res.render('index', {
        titulo: 'Este será o título da página',
        h1: 'Título do Formulário',
        numeros: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    });
    return;
};

exports.trataPost = (req, res) => {
    res.send(req.body);
    return
}