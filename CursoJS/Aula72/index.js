function Produto(nome, preco, estoque) {

    Object.defineProperties(this, {
        nome: {
            enumerable: true, // mostra a chave
            /* value: nome, // valor da chave
            writable: true, // permite alterar o valor */
            configurable: false, // permite reconfigurar ou apagar a chave
            get: function() {
                return estoque;
            },
            set: function(valor) {
                if (typeof valor !== 'number') {
                    console.log('Bad value');
                }
            },

        },
        preco: {
            enumerable: true, // mostra a chave
            /* value: preco, // valor da chave
            writable: true, // permite alterar o valor */
            configurable: false // permite reconfigurar ou apagar a chave

        },
        estoque: {
            enumerable: true, // mostra a chave
            /*   value: estoque, // valor da chave
              writable: true, // permite alterar o valor */
            configurable: false // permite reconfigurar ou apagar a chave
        }
    });



}

const p1 = new Produto('Camiseta', 20, 30);
p1.estoque = 1000;
console.log(p1);

console.log(Object.keys(p1));


function criaProduto(nome) {
    return {
        get nome() {
            return nome;
        },
        set nome(valor) {
            nome = valor;
        }
    }
}

const p2 = criaProduto('Camiseta');
console.log(p2);
p2.nome = 'Sapato';
console.log(p2);
console.log(p2.nome);