/**
 *  forEach somente em array
 */


const numeros = [1, 2, 3, 4, 5, 6, 7, 8];

numeros.forEach(function(valor, indice, array) {

    console.log(valor, indice);
});

let total = 0;
numeros.forEach(function(valor, indice, array) {
    total += valor;

});


console.log(total);