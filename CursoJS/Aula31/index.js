/* let nome = 'Luiz';
var nome2 = 'Luiz';
let nome2 = 'Luiz'; */

/* let tem escopo de bloco { ... bloco}
   var só tem escopo de função */


/* const verdadeira = true;

let nome = 'Ladislau';
var nome2 = 'Luiz';

if (verdadeira) {
    //let nome = 'Scheffer';

    if (verdadeira) {
        //let nome = 'Perrony';
        console.log(nome, nome2);
    }
} 

var sobrenome = 'Perrony';

function falaOi() {
    let outronome = 'Scheffer';
    var nome = 'Ladislau'
}

console.log(sobrenome, outronome);

falaOi();

*/

console.log(sobrenome, nome);

var sobrenome = 'Perrony';
let nome = 'Ladislau';