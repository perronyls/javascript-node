function retornaFuncao(nome) {
    return function() {
        return nome;
    }
}

const funcao = retornaFuncao('Perrony');
const funcao2 = retornaFuncao('Scheffer');

console.dir(funcao, funcao2);