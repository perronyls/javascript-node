const a1 = [1, 2, 3];
const a2 = [4, 5, 6];
//const a3 = a1 + a2;

// gera uma string

//const a3 = a1.concat(a2, [7, 8, 9], 'Perrony');
// [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 'Perrony' ]

//const a3 = a2.concat(a2, a1, [7, 8, 9]);
//[ 4, 5, 6, 4, 5, 6, 1, 2, 3, 7, 8, 9 ]

//const a3 = [...a1, ...a2, ...[7, 8, 9]];
//[ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]

//const a3 = [...a1, ...a2, [7, 8, 9]];
//[ 1, 2, 3, 4, 5, 6, [ 7, 8, 9 ] ]

//const a3 = [a1, a2, 'Perrony'];
//[ [ 1, 2, 3 ], [ 4, 5, 6 ], 'Perrony' ]

//const a3 = [...a1, ...a2, 'Perrony'];
//[ 1, 2, 3, 4, 5, 6, 'Perrony' ]
console.log(a3);