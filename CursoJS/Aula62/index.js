const nomes = ['Eduardo', 'Maria', 'Joana'];
/* console.log(nomes);
nomes[2] = 'João';
console.log(nomes);
delete nomes[2];
console.log(nomes);

// array por referência

const novo = nomes;
console.log(novo);
const removido = novo.pop();
console.log(nomes, removido);

const outro = [...novo];
console.log(novo);
console.log(outro);
outro.push('Fernanda');
console.log(novo);
console.log(outro);


outro.shift();
console.log(outro); */

nomes.unshift('João');

// suporta valores negativos
/* const novo = nomes.slice(1, 3);

console.log(novo); */

const nome = 'Ladislau Scheffer Perrony';
const palavras = nome.split(' ');
console.log(palavras);
const meuNome = palavras.join(' ');
console.log(meuNome);