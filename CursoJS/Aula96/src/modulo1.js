const nome = 'Ladislau';
const sobrenome = 'Perrony';
const idade = 55;

function soma(x, y) {
    return x + y;
}

export { nome as nome2, sobrenome, idade, soma };


export class Pessoa {
    constructor(nome, sobrenome) {
        this.nome = nome;
        this.sobrenome = sobrenome;
    }
}

export default (x, y) => x * y;