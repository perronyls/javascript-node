const numero = Number(prompt('Digite um número:'));

let numeroTitulo = document.getElementById('numero')
let raiz = document.getElementById('raiz');
let inteiro = document.getElementById('inteiro');
let nan = document.getElementById('nan');
let floorId = document.getElementById('floorId');
let ceilId = document.getElementById('ceilId');
let roundId = document.getElementById('roundId');



numeroTitulo.innerHTML = numero;
raiz.innerHTML = `Raiz quadrada: ${Math.sqrt(numero)}`;
inteiro.innerHTML = `${numero} é inteiro: ${Number.isInteger(numero)}`;
nan.innerHTML = `É NaN: ${ Number.isNaN(numero) }`;
floorId.innerHTML = `Arredondamento para baixo: ${ Math.floor(numero) }`;
ceilId.innerHTML = `Arredondamento para cima: ${ Math.ceil(numero) }`;
roundId.innerHTML = `Com duas casas decimais: ${ numero.toFixed(2) }`;