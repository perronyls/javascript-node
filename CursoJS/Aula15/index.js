let num1 = 9.54586;

// arredonda para baixo
// let num2 = Math.floor(num1);
// arredonda para cima
// let num2 = Math.ceil(num1);
// arredonda para o mais próximo inteiro
// let num2 = Math.round(num1);
// console.log(num2);

// maior numero de uma sequencia
// console.log(Math.max(1, 4, 9, 55, 34, 66));
// menor numero de uma sequencia
// console.log(Math.min(1, 4, 9, 55, 34, 66));

// const aleatorio = Math.round(Math.random() * (10 - 5) + 5);

// console.log(aleatorio);

// infinity e é true
console.log(100 / 0);