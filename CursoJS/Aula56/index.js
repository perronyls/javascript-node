/** 
 * Factory function (Função fábrica)
 */


function criaPessoa(nome, sobrenome, altura, peso) {
    return {
        nome,
        sobrenome,
        //getter
        get nomeCompleto() {
            return `${this.nome} ${this.sobrenome}`;
        },
        //setter
        set nomeCompleto(valor) {
            valor = valor.split(' ');
            this.nome = valor.shift();
            this.sobrenome = valor.join(' ');
        },

        fala: function(assunto) {
            return `${this.nome} está ${assunto}`;
        },
        altura,
        peso,
        // getter
        get imc() {
            const indice = this.peso / (this.altura ** 2);
            return indice.toFixed(2);
        }
    }
}

const p1 = criaPessoa('Ladislau', 'Perrony', 1.7, 80);
const p2 = criaPessoa('Regina', 'Correa', 1.65, 70);





console.log(p1.fala('falando sobre JS'));
p1.nomeCompleto = 'Maria Oliveira Silva';

console.log(p1.imc);
console.log(p1.nomeCompleto);

console.log(p2.fala('falando sobre Artesanato'));
console.log(p2.imc);
console.log(p2.nomeCompleto);