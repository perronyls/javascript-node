/**
 * IIFE - Imediately invoked function expression
 */

/* function qualquerCoisa() {
    console.log(111232564);
}
qualquerCoisa(); */

(function(idade, peso, altura) {
    const sobrenome = 'Scheffer';

    function criaNome(nome) {
        return nome + ' ' + sobrenome;
    }

    function falaNome() {
        console.log(criaNome('Ladislau'));
    }
    falaNome();
    console.log(idade, peso, idade);
})(55, 73, 1.70);