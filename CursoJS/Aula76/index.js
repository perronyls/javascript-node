// Produto -> acrescimo, desconto
// Camiseta, Caneca

function Produto(nome, preco) {
    this.nome = nome;
    this.preco = preco;
}

Produto.prototype.acrescimo = function(acres) {
    this.preco += acres;
}

Produto.prototype.desconto = function(desc) {
    this.preco -= desc;
}

function Camiseta(nome, preco, cor) {
    Produto.call(this, nome, preco);
    this.cor = cor;
}

Camiseta.prototype = Object.create(Produto.prototype);
Camiseta.prototype.constructor = Camiseta;

Camiseta.prototype.acrescimo = function(percentual) {
    this.preco = this.preco + (this.preco * (percentual / 100));
}

function Caneca(nome, preco, material, estoque) {
    Produto.call(this, nome, preco);
    this.material = material;
    Object.defineProperty(this, 'estoque', {
        enumerable: true,
        configurable: false,
        get: function() {
            return estoque
        },
        set: function(valor) {
            if (typeof valor !== 'Number') return;
            estoque = valor;
        }
    })
}

Caneca.prototype = Object.create(Produto.prototype);
Caneca.prototype.constructor = Caneca;

const camiseta = new Camiseta('Regata', 7.5, 'Preta');
const caneca = new Caneca('Caneca', 13, 'Plástico', 5);
const produto = new Produto('Blusa', 25);

console.log(caneca);
console.log(camiseta);
console.log(produto);