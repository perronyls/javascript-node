//Ladislau Scheffer Perrony tem 30 anos, pesa 83
//tem 1.8 de altura e seu IMC é de 25.923232
//Ladislau nasceu em 1963; 

const nome = "Ladislau ";
const sobrenome = "Scheffer Perrony";
const idade = 55;
const peso = 83;
const alturaEmCm = 1.80;
let indiceMassaCorporal = peso / Math.pow(alturaEmCm, alturaEmCm);
let anoNascimento;

let hoje = new Date();
anoNascimento = hoje.getFullYear() - idade;

console.log(`${nome} ${sobrenome} tem ${idade} anos pesa ${peso}kg`);
console.log(`tem ${alturaEmCm}mt altura e seu IMC é de ${indiceMassaCorporal}`);
console.log(`${nome}, ${sobrenome} nasceu em ${anoNascimento}`);