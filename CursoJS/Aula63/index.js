const nomes = ['Maria', 'João', 'Eduardo', 'Gabriel', 'Julia'];

//nomes.splice(indiceInicial, deleteCount: Number, ...itens para inserir a partir do índice indicado);

const removidos = nomes.splice(4, 1, 'Luiz', 'Amanda');
console.log(nomes);
console.log(removidos);