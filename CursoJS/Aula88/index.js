/* function rand(min, max) {
    min *= 1000;
    max *= 1000;
    return Math.floor(Math.random() * (max - min) + min);
}


function esperaAi(msg, tempo, cb) {
    setTimeout(() => {
        console.log(msg);
        if (cb) cb();
    }, tempo);
}

esperaAi('Frase1', rand(1, 3), function() {
    esperaAi('Frase2', rand(1, 3), function() {
        esperaAi('Frase3', rand(1, 3));
    });

}); */


function rand(min, max) {
    min *= 1000;
    max *= 1000;
    return Math.floor(Math.random() * (max - min) + min);
}

function esperaAi(msg, tempo) {
    return new Promise((resolve, reject) => {
        if (typeof msg !== 'string') reject('BAD VALUE');
        setTimeout(() => {
            resolve(msg)
        }, tempo);
    });

}

esperaAi('Conexão com BD', rand(1, 3))
    .then(resposta => {
        console.log(resposta);
        return esperaAi('Buscando dados na BASE', rand(1, 3))
    }).then(resposta => {
        console.log(resposta)
        return esperaAi(12345, rand(1, 3));
    }).then(resposta => {
        console.log(resposta);
    }).then(() => {
        console.log('Exibindo os dados!');
    })
    .catch(e => {
        console.log('ERRO', e);
    });



console.log('Antes de qualquer promisse');