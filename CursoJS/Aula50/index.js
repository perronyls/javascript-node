/**
 * arguments que sustenta todos os argumentos enviados (um objeto)
    Caso os parametros forem enviados em quantidade menor que o esperado, os demais serão definidos como undefined
    Se não colocar nenhum parâmetro cai no objeto arguments
 *  */
/* function funcao() {

    for (let argumento of arguments) {
        console.log(argumento);
    }
    console.log(arguments);
}

funcao('Valor', 'teste', 'outro'); */

/* function funcao(a, b = 2, c = 4) {
    console.log(a + b + c);
}

funcao(2, undefined, 10); */

/* function funcao({ nome, sobrenome, idade }) {
    console.log(nome, sobrenome, idade);
}

let obj = { nome: 'Ladislau', sobrenome: 'Perrony', idade: 50 };
funcao(obj);
//funcao({ nome: 'Ladislau', sobrenome: 'Perrony', idade: 50 })
 */

/* function funcao([valor1, valor2, valor3]) {
    console.log(valor1, valor2, valor3);
}

let array = [1, 2, 3];
funcao(array); */

function conta(operador, acumulador, ...numeros) {
    for (let numero of numeros) {
        if (operador === '+') acumulador += numero;
        if (operador === '-') acumulador -= numero;
        if (operador === '/') acumulador /= numero;
        if (operador === '*') acumulador *= numero;

    }
    console.log(acumulador);
    console.log(arguments);
}

conta('+', 1, 20, 30, 40, 50);