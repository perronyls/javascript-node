const { cpfs,ips } = require('./base');

const cpfRegExp = /(\d{3}\.){2}\d{3}-\d{2}/g;
/* console.log(cpfs);
console.log(cpfs.match(/[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}/g));
console.log(cpfs.match(/\d{3}\.\d{3}\.\d{3}-\d{2}/g));
console.log(cpfs.match(/(\d{3}\.){2}\d{3}-\d{2}/g)); */

/* const ipRegExp = /((25[0-5]|2[0-4][0-9]|1\d{2}|\d{2}|\d)(\.)){3}(25[0-5]|2[0-4][0-9]|1\d{2}|\d{2}|\d)/g;  */
/* const ip = '172.18.0.2'; */
const ipRegExp = /((25[0-5]|2[0-4][0-9]|1\d{2}|\d{2}|\d)(\.|$)){4}/g;

for (let i =0; i< 270; i++){
  const ip = `${i}.${i}.${i}.${i}`;
  console.log(ip, ip.match(ipRegExp));
}

console.log(ip.match(ipRegExp));