// encontra todas as palavras
const palavrasRegex = /([\wÀ-ú]+)/gi;

// Não números
const naoNumerosRegex = /\D/g

// Valida IP
const ipRegExp =  /((25[0-5]|2[0-4][0-9]|1\d{2}|\d{2}|\d)(\.|$)){4}/g;

// CPF
const cpfRegExp = /(?:(\d{3})(\.|-)){3}\d{2}/g

// Validação telefones
const phoneRegExp = /^(\()?(\d{2})?(\))?(\s)?(9)?(\s)?(\d{4})(-)?(\d{4})$/g;

// validar senhas fortes
const validaSenha = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%&\(\)\[\]]).{8,}$/g;

// validar emails
const validaEmailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gm