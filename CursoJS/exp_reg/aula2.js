const { texto } = require('./base');

const regExp1 = /Polícia Civil/gi;

// $1  $2  $3    $4  $5
// (...()) (...) (...())
/* console.log(texto); 

console.log(texto.match(regExp1)); 
console.log(texto.replace(/(Polícia)/gi,'<b>$1</b>')); */
console.log(texto.replace(/(Polícia Civil)/gi, (input) => {
    return input.toLowerCase()
}));

