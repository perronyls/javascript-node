const { cpfs2 } = require('./base');

// ^ -> começa com
// $ -> Termina com 
// [^] -> dentro de grupo é negação
// m - multiline - obedece o ^ e o $ mas em cada linha

const cpfRegExp = /^(\d{3}\.){2}\d{3}-\d{2}$/gm;
const cpf = '254.224.877-55';

console.log(cpfs2.match(cpfRegExp));