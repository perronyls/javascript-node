const { texto,arquivos } = require('./base');

// * (opcionais)
// + (obrigatório) 1 ou n
// ? (opcionais) 0 ou 1
// \ Caractere de escape
// {n,m} mínimo e máximo
// {10,} mínimo 10
// {,10} de 0 a 10
// {5,10} de 5 a 10

/* const regExp1 = /Po+lícia/gi;

console.log(texto.match(regExp1)); */

/* const regExp2 = /\.jpe?g/gi */
/* const regExp2 = /\.jpe{,1}g/gi */
const regExp2 = /\.(jp|JP)(e|E)(g|G)/g
for (const arquivo of arquivos) {
  console.log(arquivo,arquivo.match(regExp2));
}