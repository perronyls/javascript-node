const texto = `
Dois hectares e meio de plantação de coca e um laboratório para o preparo de cocaína foram localizados na Amazônia, em operação realizada na sexta-feira (14) entre o Comando Militar da Amazônia e a Polícia Civil do Amazonas.
De acordo com informações da Poooooooooooooolícia Civil, imagens de satélite localizaram a área onde a planta era cultivada e refinada, próxima à fronteira do Brasil com o Peru.
No local, foram encontrados diversos materiais e equipamentos para o processamento das folhas, como 40 litros de ácido sulfúrico, dez quilos de cal, um saco de cimento e amônia.
Durante a operação, foram utilizados três helicópteros do Exército e embarcações para chegar ao local, próximo à margem do rio Javari.
Segundo as investigações, as lavouras são encomendadas por cartéis de narcotraficantes aos agricultores ribeirinhos peruanos, colombianos e brasileiros, que moram na região. As plantações são divididas em pequenos lotes para escapar da vigilância dos satélites.
A produção de folhas colhidas é comprada por valores que variam entre R$ 3 mil e R$ 6 mil, conforme a quantidade produzida, ainda segundo a Polícia Civil.
Ninguém foi preso, mas as investigações prosseguem e os policiais civis e os militares vão permanecer na região para tentar identificar os responsáveis e descobrir se há outros locais próximos servindo para o cultivo de coca.
Duas possíveis pistas de pouso clandestino também foram localizadas nas proximidades do laboratório e, caso sejam confirmadas como área para pouso ilegal de aeronaves, serão destruídas. As equipes também vão destruir as plantações encontradas.
`;

const arquivos = [
  'Atenção.jpg',
  'FOTO.jpeg',
  'Meu gatinho.jpg',
  'Meu gatinho.jpeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeg',
  'Meu gatinho.JPG',
  'Meu gatinho.JPEG',
  'Marido.png',
  'lista de compras.txt'
];

const cpfs = `
Os CPFs são: 
837.269.890-21
538.644.990-70
250.634.050-53
737.268.490-09
`;

const cpfs2 = `
837.269.890-21
538.644.990-70
250.634.050-53
737.268.490-09
`;

const ips = `
Os Ips são: 
  0.0.0.0
  192.168.0.25
  172.18.0.1
  10.10.5.12
  255.255.255.255

`;

const alfabeto = 
'ABCDEFGHIJKLMNOPQRSTUVWXYZ  abcdefghijklmnopqrstuvwxyz @! 0123456789 !';

const html = '<p>Olá mundo</p> <p>Olá de novo</p> <div>Uma Div</div>';

const html2 = `<p class="teste"> 
Olá mundo
</p> <p>Olá de novo</p> <div>Uma Div</div>`;

const lookahead = `
ONLINE 192.168.0.1 ABCDEF inactive
OFFLINE 192.168.0.2 ABCDEF active
ONLINE 192.168.0.3 ABCDEF active
ONLINE 192.168.0.4 ABCDEF active
OFFLINE 192.168.0.5 ABCDEF active
OFFLINE 192.168.0.6 ABCDEF inactive


`

module.exports = { texto,arquivos,html,alfabeto,cpfs,ips,cpfs2,html2,lookahead }
