  const { texto } = require('./base');

  // () grupos
  // | ou 


  // g = global (encontra todas as ocorrências)
  /* const regExp1 = /João/g; */


  // i = insensitive (não respeita maiúsculas e minúsculas)
  /* const regExp1 = /joão/i; */

  // função test testa a expressão e se encontra no texto.
  /* console.log(regExp1.test(texto)); */

  const regExp1 = /(Os problemas|o problema) ((financeiros|financeiro) do Brasil)/g;
  const found = regExp1.exec(texto);

  if(found){
  console.log(found[0]);
  console.log(found[1]);
  console.log(found[2]);

  }


