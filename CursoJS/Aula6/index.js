1
/*let nome = 'João' // string;

console.log(nome, 'nasceu em 1984');
console.log('Em 2000 ', nome, ' conheceu Maria.');
console.log(nome, ' casou-se com Maria em 2012');
console.log('Maria teve 1 filho com ', nome, ' em 2015');
console.log('O filho de ', nome, ' se chama Eduardo;');
*/

let nome; // declarada a variável (undefined);
nome = 'Ladislau'; // inicializando a variável
console.log(nome);
nome = 'Scheffer'; // redefinindo o valor da variável
console.log(nome);
nome = 'Perrony'; // redefinindo o valor da variável
console.log(nome);