import styled from 'styled-components';

export const Title = styled.h1`
  span {
    font-size: 12px;
    margin-left: 15px;
    text-align: center;
  }
`;
