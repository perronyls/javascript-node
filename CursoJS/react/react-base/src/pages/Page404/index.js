import React from 'react';
import { Title } from './styled';
import { Container } from '../../styles/GlobalStyles';

export default function Page404() {
  return (
    <Container>
      <Title>
        <span>Essa página não existe</span>
      </Title>
    </Container>
  );
}
