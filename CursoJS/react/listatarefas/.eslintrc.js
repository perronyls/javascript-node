module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        'plugin:react/recommended',
        'airbnb',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'react',
    ],
    rules: {
        "react/jsx-filename-extension": "off",
        "indent": "off",
        "react/jsx-tag-spacing": "off",
        "react/jsx-one-expression-per-line": "off",
        "react/jsx-wrap-multilines": "off",
        "lines-between-class-members": "off",
        "react/jsx-closing-tag-location": "off",
        "no-trailing-spaces": "off",
        "react/state-in-constructor": "off",
        "space-before-blocks": "off",
        "react/forbid-prop-types": "off",
    },
};