/* const frutas = [
    'Pera', 'Maçã', 'Uva', 'Laranja'
]; */

/* for (let i = 0; i < frutas.length; i++) {
    console.log(frutas[i]);
} */

/* for (let indice in frutas) {
    console.log(frutas[indice]);
} */

const pessoa = {
    nome: 'Ladislau',
    sobrenome: 'Scheffer Perrony',
    idade: 55
}

for (let index in pessoa) {
    console.log(pessoa[index]);
}