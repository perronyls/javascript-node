/**
 *  Aritméticos
 *  + adição / concatenação
 *  - subtração 
 *  / divisão
 *  * multiplicação
 *  ** Potenciação
 *  % Resto da divisão
 *  
 *  Precedência (), **, *, /, %, +-
 * 
 */

const num1 = 2;
const num2 = 10;
const num3 = 5;

console.log(num1 ** num2);

let contador = 1;

console.log(contador++);
console.log(contador);

contador = 10
console.log(contador--);
console.log(--contador);

let step = 2;
contador = contador + step;
console.log(contador);

contador = contador + step;
console.log(contador);

contador = contador + step;
console.log(contador);

contador += step;
console.log(contador);