// NaN - Not a number

const num1 = 10;
const num2 = '5.2';

console.log(num1 + parseInt(num2));
console.log(num1 + parseFloat(num2));
console.log(num1 + Number(num2));
console.log(num1 + num2);