/* function getMes(mes) {
    const mesString = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Março',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    ];
    return mesString[mes];
}



function getDiaSemana(dia) {
    const dias = [
        'Domingo',
        'Segunda-feira',
        'Terça-feira',
        'Quarta-feira',
        'Quinta-feira',
        'Sexta-feira',
        'Sábado'
    ]
    return dias[dia];
}

function setResultado(data) {
    let dataTexto = document.querySelector('#data');
    let p = document.createElement('p');
    dataTexto.appendChild(p);
    p.innerHTML = `${getDiaSemana(data.getDay())}, ${data.getDate()} de ${getMes(data.getMonth() +1)} de ${data.getFullYear()} ${data.getHours()}:${data.getMinutes()}`;


}

function mostraData() {
    // const data = new Date(2019, 9, 7, 22, 52);
    const data = new Date();
    setResultado(data);


}

mostraData(); */

const h1 = document.querySelector('.container h1');
const data = new Date();
const opcoes = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
}

h1.innerHTML = data.toLocaleDateString('pt-BR', opcoes);