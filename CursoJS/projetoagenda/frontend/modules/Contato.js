import validator from 'validator';

export default class Contato {
    constructor(formClass) {
        this.form = document.querySelector(formClass);


    }

    init() {

        this.events();
    }

    events() {
        if (!this.form) return;
        for (let errorText of this.form.querySelectorAll('error-text')) {
            errorText.remove();
        };

        this.form.addEventListener('submit', e => {
            e.preventDefault();
            this.validate(e);

        });

    }

    validate(e) {
        const el = e.target;
        const nomeInput = el.querySelector('input[name="nome"]');
        const emailInput = el.querySelector('input[name="email"]');
        const telefoneInput = el.querySelector('input[name="telefone"]');

        let error = false;

        if (nomeInput.value == '') {
            this.criaErro(nomeInput, 'Nome não pode ser vazio!')
            error = true;
        }

        if (emailInput.value && !validator.isEmail(emailInput.value)) {
            this.criaErro(emailInput, 'Email inválido');
            error = true;
        }

        if (!emailInput.value && !telefoneInput.value) {
            this.criaErro(telefoneInput, 'Obrigatório informar telefone ou email');
            error = true
        }


        if (!error) el.submit();

    }

    criaErro(campo, msg) {
        const div = document.createElement('div');
        div.innerHTML = msg;
        div.style.color = 'red';
        div.classList.add('error-text');
        campo.insertAdjacentElement('afterend', div);

    }


}