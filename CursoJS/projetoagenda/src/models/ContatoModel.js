const mongoose = require('mongoose');
const validator = require('validator');

const ContatoSchema = new mongoose.Schema({
    nome: { type: String, required: true },
    sobrenome: { type: String, required: false, default: '' },
    email: { type: String, required: false, default: '' },
    telefone: { type: String, required: false, defualt: '' },
    createdAt: { type: Date, default: Date.now }

});

const ContatoModel = mongoose.model('Contato', ContatoSchema);

class Contato {
    constructor(body) {
        this.body = body;
        this.errors = [];
        this.contato = null;
    }

    async delete(id) {

        if (typeof id !== 'string') return;
        if (this.errors.length > 0) return;
        this.contato = await ContatoModel.findByIdAndDelete(id);
        return;
    }


    async buscaPorId(id) {
        if (typeof id !== 'string') return;
        const contato = await ContatoModel.findById({ _id: id });
        return contato;
    }

    async edit(id) {

        if (typeof id !== 'string') return;
        this.valida();
        if (this.errors.length > 0) return;
        this.contato = await ContatoModel.findByIdAndUpdate(id, this.body, { new: true });


    }

    async register() {

        this.valida();
        if (this.errors.length > 0) return;

        await this.contatoExists();

        if (this.errors.length > 0) return;

        this.contato = await ContatoModel.create(this.body);

    }

    async contatoExists() {
        this.contato = await ContatoModel.findOne({ email: this.body.email });

        if (this.contato) this.errors.push('Contato já existe!')
    }

    valida() {
        this.cleanUp();

        if (this.body.nome.length < 3) this.errors.push('Nome deve conter no mínimo 3 caracteres');
        if (this.body.email && !validator.isEmail(this.body.email)) this.errors.push('E-mail inválido');
        if (!this.body.email && !this.body.telefone) this.errors.push('Obrigatório informar telefone ou email')

    }

    cleanUp() {
        for (const key in this.body) {
            if (typeof this.body[key] !== 'string') {
                this.body[key] = '';
            }
        }
        this.body = {
            nome: this.body.nome,
            sobrenome: this.body.sobrenome,
            email: this.body.email,
            telefone: this.body.telefone,
            createdAt: this.body.createdAt
        };

    }

    async buscaContatos() {
        const contatos = await ContatoModel.find().sort({ createdAt: -1 });
        return contatos;
    }


}

module.exports = Contato;