const Contato = require('../models/ContatoModel');

exports.index = (req, res) => {
    res.render('contato', {
        contato: {}
    });
};

exports.register = async function(req, res) {
    try {
        const contato = new Contato(req.body);
        await contato.register();

        if (contato.errors.length > 0) {
            req.flash('errors', contato.errors);
            req.session.save(function() {
                return res.redirect('back');
            })
            return;
        }

        req.flash('success', 'Contato registrado com Sucesso');
        req.session.save(() => res.redirect(`/contato/index/${contato.contato._id}`));

        return;

    } catch (error) {
        console.log(error);
    }

}

exports.editIndex = async function(req, res) {
    try {
        const Obj = new Contato();
        if (!req.params.id) return res.render('404');
        const contato = await Obj.buscaPorId(req.params.id);
        if (!contato) return res.render('404');

        res.render('contato', { contato });

    } catch (error) {
        console.log(error);
    }

}

exports.edit = async function(req, res) {
    try {
        if (!req.params.id) return res.render('404');
        const contato = new Contato(req.body);
        await contato.edit(req.params.id);


        if (contato.errors.length > 0) {
            req.flash('errors', contato.errors);
            req.session.save(function() {
                return res.redirect('back');
            })
            return;
        }

        req.flash('success', 'Contato atualizado com Sucesso');
        req.session.save(() => res.redirect(`/contato/index/${contato.contato._id}`));

        return;


    } catch (error) {
        console.log(error);
    }



}

exports.delete = async function(req, res) {
    try {
        if (!req.params.id) return res.render('404');
        const contato = new Contato(req.body);
        await contato.delete(req.params.id);


        if (contato.errors.length > 0) {
            req.flash('errors', contato.errors);
            req.session.save(function() {
                return res.redirect('back');
            })
            return;
        }

        req.flash('success', 'Contato excluido com Sucesso');
        req.session.save(() => res.redirect('back'));
        return;


    } catch (error) {
        console.log(error);
    }


}